const ZooKeeper = require('zookeeper');

class ZooKeeperQueue {

    constructor(nameQueue, connect='127.0.0.1:2181', timeout=5000 ) {
        this.pathQueue = `/${nameQueue.replace("/", "")}`;
        this.pathTasksQueue = `${this.pathQueue}/tasks`;
        this.pathTasksAtWorkQueue = `${this.pathQueue}/tasks_at_work`;

        this.zooKeeperClient = new ZooKeeper({
            connect: connect,
            timeout: timeout,
            debug_level: ZooKeeper.ZOO_LOG_LEVEL_WARN,
            host_order_deterministic: false,
        });

        this.queue = new Promise((resolve, reject) => {
                this.zooKeeperClient.on('connect', async () => {
                    try {
                        await this.zooKeeperClient.exists(this.pathQueue).catch(
                            async () => {
                                await this.zooKeeperClient.create(this.pathQueue, '', ZooKeeper.ZOO_CONTAINER);
                                await this.zooKeeperClient.create(this.pathTasksQueue, '', ZooKeeper.ZOO_CONTAINER);
                                await this.zooKeeperClient.create(this.pathTasksAtWorkQueue, '', ZooKeeper.ZOO_CONTAINER)
                            }
                        );
                        resolve(this);
                    } catch (e) {
                        reject();
                    }
                });
            });
    }

    getQueue() {
        this.zooKeeperClient.init({});
        return this.queue
    }

    async enqueue(task) {
        const pathTaskQueue = `${this.pathTasksQueue}/t-`;
        const data = JSON.stringify({'task': task, 'done': false});
        await this.zooKeeperClient.create(pathTaskQueue, data, ZooKeeper.ZOO_SEQUENCE);
    }

    async _getFreeTasks() {
        const tasks = await this.zooKeeperClient.get_children(`${this.pathTasksQueue}`);
        const tasksAtWork = await this.zooKeeperClient.get_children(`${this.pathTasksAtWorkQueue}`);
        return tasks.filter(task => !tasksAtWork.includes(task));
    }

    async _getSortedFreeTasks() {
        const freeTasks = await this._getFreeTasks()
        return freeTasks.sort((str1,str2)=>str2.localeCompare(str1));
    }

    async isEmpty() {
        return (await this._getFreeTasks()).length === 0;
    }

    async dequeue() {
        let sortedFreeTasks = await this._getSortedFreeTasks();
        while (sortedFreeTasks.length !== 0) {
            try {
                const task = sortedFreeTasks.pop();
                //якщо вузел вже існую то буде виключення, тобто таску інший воркер не візьме
                await this.zooKeeperClient.create(`${this.pathTasksAtWorkQueue}/${task}`, '', ZooKeeper.ZOO_EPHEMERAL);
                const taskData = await this.zooKeeperClient.get(`${this.pathTasksQueue}/${task}`);
                return {'id': task, 'task': JSON.parse(taskData[1].toString()).task};
            } catch (e) {
                sortedFreeTasks = await this._getSortedFreeTasks();
            }
        }
        return {};
    }

    async taskDone(idTask) {
        await this.zooKeeperClient.delete_(`${this.pathTasksQueue}/${idTask}`, -1);
        await this.zooKeeperClient.delete_(`${this.pathTasksAtWorkQueue}/${idTask}`, -1);
    }

    async close(){
        this.zooKeeperClient.close()
    }
}

(async () => {
    const zooKeeper = new ZooKeeperQueue('Queue');
    const queue = await zooKeeper.getQueue();
    queue.enqueue({'dddd': 22222, 'qffff': 'ffffq'});
    const {id, task} = await queue.dequeue();
    console.log(id, task);
    if (id)
        await queue.taskDone(id);

    queue.close();

})();

